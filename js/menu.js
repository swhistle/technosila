var button = document.querySelector(".b-header-block__button-menu");
var menu = document.querySelector(".b-user-nav");

button.addEventListener("click", function(event) {
    event.preventDefault();
    menu.classList.toggle("b-user-nav--show");
    button.classList.toggle("b-header-block__button-menu--close");
})