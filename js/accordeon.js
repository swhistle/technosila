if ($( document ).width() <= 960) {

    $(document).ready(function() {
        $('.b-accordeon__trigger').on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                item = $this.closest('.b-accordeon__item'),
                list = $this.closest('.b-accordeon__list'),
                items = list.find('.b-accordeon__item'),
                content = item.find('.b-accordeon__inner'),
                otherContent = list.find('.b-accordeon__inner'),
                duration = 300;

                if (!item.hasClass('b-accordeon__item--active')) {
                    items.removeClass('b-accordeon__item--active');
                    item.addClass('b-accordeon__item--active');

                    otherContent.stop(true).slideUp(duration);
                    content.stop(true).slideDown(duration); 
                } else {
                    content.stop(true).slideUp(duration);
                    item.stop(true).removeClass('b-accordeon__item--active');
                }

                

        });

    });

}