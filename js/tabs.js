function openTab(evt, tab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("b-tab-content");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("b-tabs__link");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" b-tabs__link--active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " b-tabs__link--active";
}

document.getElementById("defaultOpen").click();