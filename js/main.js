//СЛАЙДЕР В ХЕДЕРЕ (Slick)
var widthW = $( document ).width();

if (widthW > 960) {
    var x = true,
        amountBrand = 6,
        amountProductRecom = 4,
        amountProductRecent = 4;
} else if ((widthW <= 960) && (widthW > 600)) {
    var x = false,
        amountBrand = 3,
        amountProductRecom = 3,
        amountProductRecent = 3;
} else if (widthW <= 600) {
    var x = false,
        amountBrand = 3,
        amountProductRecom = 2,
        amountProductRecent = 2;
}

$(function() {

	$('.b-slideshow').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
        arrows: x,     
        dots: true,		
	});
});

//БРЕНДЫ

$(function(){

  $('.b-carousel__items--brand').slick({
        infinite: true,
        slidesToShow: amountBrand,
        slidesToScroll: 1,
        arrows: x,  
  });
});

//ТОВАРЫ

$(function(){

  $('.b-carousel__items--product').slick({
        infinite: true,
        slidesToShow: amountProductRecom,
        slidesToScroll: 1,
        arrows: x,  
  });
});

//НЕДАВНО ПРОСМОТРЕННЫЕ ТОВАРЫ

$(function(){

  $('.b-carousel__items--recently-viewed').slick({
        infinite: true,
        slidesToShow: amountProductRecent,
        slidesToScroll: 1,
        arrows: x,  
  });
});