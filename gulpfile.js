'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var pug = require('gulp-pug');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('style', function() {
    gulp.src('less/style.less' )
        .pipe(less())
        .pipe(autoprefixer({ browsers: ['IE 8', 'IE 9', 'last 5 versions', 'Firefox 14', 'Opera 11.1'] })) //???
        .pipe(gulp.dest('css'));
});

/*gulp.task('pages', function() {
    return gulp.src('pug/main.pug')
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest('public'));

});*/

gulp.task('watch', function() {
    gulp.watch('less/style.less', ['style']);
    //gulp.watch('pug/main.pug', ['pages']);

});



gulp.task('default', [/*'pages', */'style', 'watch']);